// 1.Це процес додавання спеціальних символів або кодів до даних, щоб запобігти сприйняттю певних символів або послідовностей як коду мовою програмування або програмним додатком. 
// У мовах програмування екранування необхідне для того, щоб будь-які вхідні дані користувача, які  обробляються програмою, розглядалися як дані, а не як виконуваний код.
// 2.Function Declaration, Function Expression.
// 3.Це термін, що використовується  для опису переміщення оголошень змінних і функцій на початок відповідних областей видимості на етапі складання коду, до того, як код буде виконано.
function createNewUser() {
    let firstName = prompt("Enter your first name:");
    let lastName = prompt("Enter your last name:");
    let dateOfBirth = prompt("Enter your date of birth (dd.mm.yyyy):");
    
    let newUser = {
      firstName: firstName,
      lastName: lastName,
      birthday: dateOfBirth,
      getAge: function() {
        let today = new Date();
        let birthdate = new Date(this.birthday.split(".").reverse().join("-"));
        let age = today.getFullYear() - birthdate.getFullYear();
        let month = today.getMonth() - birthdate.getMonth();
        if (month < 0 || (month === 0 && today.getDate() < birthdate.getDate())) {
          age--;
        }
        return age;
      },
      getPassword: function() {
    return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.split(".")[2]);
      }
    };
    return newUser;
  }
  let user = createNewUser();
  console.log(user);
  console.log("Age:", user.getAge());
  console.log("Password:", user.getPassword());